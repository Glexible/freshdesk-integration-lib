<?php

namespace Freshdesk\Models\SubModels;

/**
 * Class KeyValue
 * @package Freshdesk\Models\SubModels
 */
class KeyValue extends SubModelBase
{
    /** @var string */
    public $key;

    /** @var string */
    public $value;
    
    /**
     * @return array
     */
    public function exportNotEmpty()
    {
        return [$this->key => $this->value];
    }
}