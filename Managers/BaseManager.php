<?php

namespace Freshdesk\Managers;

use Freshdesk\Models\SubModels\Attachment;
use Freshdesk\Client;
use Freshdesk\Config;
use Freshdesk\Models\BaseModel;
use Guzzle\Http\Exception\RequestException;
use Guzzle\Http\Message\Response;

/**
 * Class BaseManager
 * @package Freshdesk\Managers
 */
class BaseManager
{
    /** @var Config */
    protected $config;

    /** @var Client */
    protected $client;

    /** @var  Response */
    protected $response;

    /** @var string */
    protected $objectUrl;
    
    /** @var array */
    protected $filters = [];

    /**
     * these are used for creation request url
     *
     * @var array
     */
    protected $parentObjectIds;

    /**
     * it's used for creation output Model after request to freshdesk.com
     *
     * @var string
     */
    protected $modelName;

    const OBJECT_URL = '';
    
    const ORDER_BY = 'order_by';
    const ORDER_TYPE = 'order_type';
    const ORDER_TYPE_ASC = 'asc';
    const ORDER_TYPE_DESC = 'desc';
    
    /**
     * BaseManager constructor.
     */
    public function __construct()
    {
        $this->modelName = 'Freshdesk\\Models' .
            (empty($this->modelName)
                ? str_replace([__NAMESPACE__, 'Manager'], '', get_class($this))
                : '\\' . $this->modelName
            )
        ;

        $this->setObjectUrl(static::OBJECT_URL);
    }

    /**
     * @param array $settings
     *
     * @return $this
     * @throws \Exception if required property is empty
     */
    public function init($settings)
    {
        $this->config = new Config($settings);
        $this->client = new Client($this->config);

        return $this;
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param array $settings
     *
     * @return bool
     * @throws \Exception  Exception during http request to freshdesk.com
     */
    public function testConnection($settings = [])
    {
        if (!empty($settings)) {
            $this->init($settings);
        }

        $response = $this->client->execute('settings/helpdesk', Client::ACTION_LIST);
        $remoteSettings = $response->json();

        return !empty($remoteSettings['primary_language']);
    }
    
    /**
     * @param int|array $parentObjectIds
     *
     * @return $this
     */
    public function setParentObjectIds($parentObjectIds)
    {
        if (is_array($parentObjectIds)) {
            array_map('intval', $parentObjectIds);
        } else {
            $parentObjectIds = [(int)$parentObjectIds];
        }

        $this->parentObjectIds = $parentObjectIds;
        
        return $this;
    }

    /**
     * It's used in self::getFullObjectUrl() for building url
     *
     * @return array
     */
    public function getParentObjectIds()
    {
        return $this->parentObjectIds;
    }

    /**
     * @return string
     */
    public function getObjectUrl()
    {
        return $this->objectUrl;
    }

    /**
     * @param string $objectUrl
     */
    public function setObjectUrl($objectUrl)
    {
        $this->objectUrl = $objectUrl;
    }

    /**
     * @return string
     */
    protected function getFullObjectUrl()
    {
        if (empty($this->parentObjectIds)) {
            return $this->addFiltersToUrl($this->objectUrl);
        }

        $params = $this->parentObjectIds;
        array_unshift($params, $this->objectUrl);
        $url = call_user_func_array('sprintf', $params);
        
        return $this->addFiltersToUrl($url);
    }
    
    /**
     * @param string $url
     *
     * @return string
     */
    private function addFiltersToUrl($url)
    {
        return $url . (empty($this->filters) ? '' : '?' . http_build_query($this->filters));
    }

    /**
     * General method that creates|updates|deletes|gets list|gets object from freshdesk.com API
     * it's called by appropriate public methods
     *
     * @param string       $action
     * @param array        $data
     * @param null|int     $id
     * @param Attachment[] $attachments
     *
     * @return BaseModel|array|bool
     * @throws \Exception                Exception during http request to freshdesk.com
     */
    protected function manageObjects($action, $data = array(), $id = null, $attachments = [])
    {
        $url = $this->getFullObjectUrl();

        try {
            $this->response = $this->client->execute($url, $action, $data, $id, $attachments);
        } catch (RequestException $e) {
            throw new \Exception(
                'Url: ' . $url . "\nAction: " . $action . "Data: " . var_export($data, true)
                . "\nID: " . var_export($id, true)
                . "\nException: " . $e->getMessage()
                . "\n\nResponseBody: " . (is_object($e->getRequest()->getResponse()) ? $e->getRequest()->getResponse()->getBody(true) : '')
            );
        } catch (\Exception $e) {
            throw new \Exception(
                'Url: ' . $url . "\nAction: " . $action . "Data: " . var_export($data, true)
                . "\nID: " . var_export($id, true)
                . "\nException: " . $e->getMessage()
            );
        }

        switch ($action) {
            case Client::ACTION_SHOW :
            case Client::ACTION_CREATE :
            case Client::ACTION_UPDATE :
                /** @var BaseModel $modelObject */
                $modelObject = new $this->modelName();

                return $modelObject->fillObject($this->response->json());
            case Client::ACTION_DELETE :
                return $this->response->getStatusCode() === 204;
            case Client::ACTION_LIST :
                return $this->response->json();
        }

        return null;
    }
    
    /**
     * @param array $filters
     *
     * @return $this
     */
    public function setFilters(array $filters)
    {
        $this->filters = $filters;
        
        return $this;
    }

    /**
     * @param BaseModel $modelObject
     *
     * @return BaseModel
     * @throws \Exception Exception during http request to freshdesk.com
     */
    public function create($modelObject)
    {
        return $this->manageObjects(
            Client::ACTION_CREATE,
            $modelObject->exportData(),
            null,
            $modelObject->hasAttachment() ? $modelObject->getAttachments() : []
        );
    }

    /**
     * @param BaseModel $modelObject
     * @param int       $id
     *
     * @return BaseModel
     * @throws \Exception Exception during http request to freshdesk.com
     */
    public function update($modelObject, $id)
    {
        return $this->manageObjects(
            Client::ACTION_UPDATE,
            $modelObject->exportData(),
            $id,
            $modelObject->hasAttachment() ? $modelObject->getAttachments() : []
        );
    }

    /**
     * @param int $id
     *
     * @return bool
     * @throws \Exception Exception during http request to freshdesk.com
     */
    public function delete($id)
    {
        return $this->manageObjects(Client::ACTION_DELETE, array(), $id);
    }

    /**
     * @param array $filters
     *
     * @return BaseModel[]
     * @throws \Exception Exception during http request to freshdesk.com
     */
    public function getList($filters = array())
    {
        $results = $this->setFilters($filters)->manageObjects(Client::ACTION_LIST);
        $this->setFilters([]);//reset filters
        $models = [];
        
        foreach ($results as $item) {
            /** @var BaseModel $modelObject */
            $modelObject = new $this->modelName();
            $models[] = $modelObject->fillObject($item);
        }

        return $models;
    }

    /**
     * @param int $id
     *
     * @return BaseModel
     * @throws \Exception Exception during http request to freshdesk.com
     */
    public function getOne($id)
    {
        return $this->manageObjects(Client::ACTION_SHOW, array(), $id);
    }

    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * @param array $settings
     *
     * @return static
     * @throws \Exception
     */
    public static function getManager($settings)
    {
        static $manager = null;
        
        if ($manager === null) {
            $manager = new static;
            $manager->init($settings);
        }

        return $manager;
    }
}