<?php

namespace Freshdesk\Models;

use Freshdesk\Models\SubModels\Avatar;

/**
 * Class Contact
 * @package Freshdesk\Models
 */
class Contact extends BaseModel
{
    /**
     * Set to true if the contact has been verified
     *
     * @var bool
     */
    protected $active;
    
    /**
     * Address of the contact
     *
     * @var string
     */
    protected $address;
    
    /**
     * 	Avatar of the contact
     *
     * @var Avatar
     */
    protected $avatar;
    
    /**
     * ID of the primary company to which this contact belongs
     *
     * @var int
     */
    protected $company_id;
    
    /**
     * Set to true if the contact can see all tickets that are associated with the company to which he belong
     *
     * @var bool
     */
    protected $view_all_tickets;
    
    /**
     * Key value pair containing the name and value of the custom fields.
     *
     * @var array
     */
    protected $custom_fields;
    
    /**
     * Set to true if the contact has been deleted. Note that this attribute will only be present for deleted contacts
     *
     * @var bool
     */
    protected $deleted;
    
    /**
     * A short description of the contact
     *
     * @var string
     */
    protected $description;
    
    /**
     * Primary email address of the contact.
     * If you want to associate additional email(s) with this contact, use the other_emails attribute
     *
     * @var string
     */
    protected $email;
    
    /**
     * Job title of the contact
     *
     * @var string
     */
    protected $job_title;
    
    /**
     * Language of the contact
     *
     * @var string
     */
    protected $language;
    
    /**
     * Mobile number of the contact
     *
     * @var string
     */
    protected $mobile;
    
    /**
     * Name of the contact
     *
     * @var string
     */
    protected $name;
    
    /**
     * Additional emails associated with the contact
     *
     * @var array
     */
    protected $other_emails = [];
    
    /**
     * Telephone number of the contact
     *
     * @var string
     */
    protected $phone;
    
    /**
     * Tags associated with this contact
     *
     * @var array
     */
    protected $tags;
    
    /**
     * Time zone in which the contact resides
     *
     * @var string
     */
    protected $time_zone;
    
    /**
     * Twitter handle of the contact
     *
     * @var string
     */
    protected $twitter_id;
    
    /**
     * Additional companies associated with the contact
     *
     * @var array
     */
    protected $other_companies;
    
    /** @var string */
    protected $created_at;
    
    /** @var string */
    protected $updated_at;
    
    /** @var array */
    protected $addExcludeExportProperties = ['currentEmail'];
    
    /**
     * property is used for FreshdeskService only
     *
     * @var string
     */
    protected $currentEmail;
    
    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }
    
    /**
     * @param bool $active
     *
     * @return Contact
     */
    public function setActive($active)
    {
        $this->active = $active;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }
    
    /**
     * @param string $address
     *
     * @return Contact
     */
    public function setAddress($address)
    {
        $this->address = $address;
        
        return $this;
    }
    
    /**
     * @return Avatar
     */
    public function getAvatar()
    {
        return $this->avatar;
    }
    
    /**
     * @param Avatar $avatar
     *
     * @return Contact
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }
    
    /**
     * @param int $company_id
     *
     * @return Contact
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isViewAllTickets()
    {
        return $this->view_all_tickets;
    }
    
    /**
     * @param bool $view_all_tickets
     *
     * @return Contact
     */
    public function setViewAllTickets($view_all_tickets)
    {
        $this->view_all_tickets = $view_all_tickets;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getCustomFields()
    {
        return $this->custom_fields;
    }
    
    /**
     * @param array $custom_fields
     *
     * @return Contact
     */
    public function setCustomFields($custom_fields)
    {
        $this->custom_fields = $custom_fields;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }
    
    /**
     * @param bool $deleted
     *
     * @return Contact
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * @param string $description
     *
     * @return Contact
     */
    public function setDescription($description)
    {
        $this->description = $description;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getJobTitle()
    {
        return $this->job_title;
    }
    
    /**
     * @param string $job_title
     *
     * @return Contact
     */
    public function setJobTitle($job_title)
    {
        $this->job_title = $job_title;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }
    
    /**
     * @param string $language
     *
     * @return Contact
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }
    
    /**
     * @param string $mobile
     *
     * @return Contact
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     *
     * @return Contact
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getOtherEmails()
    {
        return $this->other_emails;
    }
    
    /**
     * @param array $other_emails
     *
     * @return Contact
     */
    public function setOtherEmails($other_emails)
    {
        $this->other_emails = $other_emails;
        
        return $this;
    }
    
    /**
     * @param string $other_email
     *
     * @return Contact
     */
    public function addOtherEmails($other_email)
    {
        array_push($this->other_emails, $other_email);
        
        return $this;
    }
    
    /**
     * @param string $email
     *
     * @return bool
     */
    public function hasEmail($email)
    {
        return $this->email == $email || in_array($email, $this->other_emails);
    }
    
    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    /**
     * @param string $phone
     *
     * @return Contact
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }
    
    /**
     * @param array $tags
     *
     * @return Contact
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getTimeZone()
    {
        return $this->time_zone;
    }
    
    /**
     * @param string $time_zone
     *
     * @return Contact
     */
    public function setTimeZone($time_zone)
    {
        $this->time_zone = $time_zone;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getTwitterId()
    {
        return $this->twitter_id;
    }
    
    /**
     * @param string $twitter_id
     *
     * @return Contact
     */
    public function setTwitterId($twitter_id)
    {
        $this->twitter_id = $twitter_id;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getOtherCompanies()
    {
        return $this->other_companies;
    }
    
    /**
     * @param array $other_companies
     *
     * @return Contact
     */
    public function setOtherCompanies($other_companies)
    {
        $this->other_companies = $other_companies;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * @param string $created_at
     *
     * @return Contact
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * @param string $updated_at
     *
     * @return Contact
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        
        return $this;
    }
    
    /**
     * @param string $currentEmail
     *
     * @return $this
     */
    public function setCurrentEmail($currentEmail)
    {
        $this->currentEmail = $currentEmail;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCurrentEmail()
    {
        return $this->currentEmail;
    }
}