<?php

namespace Freshdesk\Models;

use Freshdesk\Models\Traits\Attachments;

/**
 * Class TicketReply
 * @package Freshdesk\Models
 */
class TicketReply extends BaseModel
{
    use Attachments;
    
    /** @var int */
    protected $ticket_id;
    /**
     * Content of the note in HTML format
     *
     * @var string
     */
    protected $body;
    
    /**
     * Content of the note in HTML format
     *
     * @var string
     */
    protected $body_text;
    
    /**
     * The email address from which the reply is sent. By default the global support email will be used.
     *
     * @var string
     */
    protected $from_email;
    
    /**
     * ID of the agent/user who is adding the note
     *
     * @var int
     */
    protected $user_id;
    
    /**
     * Email address added in the 'cc' field of the outgoing ticket email.
     *
     * @var array
     */
    protected $cc_emails;
    
    /**
     * Email address added in the 'bcc' field of the outgoing ticket email.
     *
     * @var array
     */
    protected $bcc_emails;
    
    /**
     * Emails array
     *
     * @var array
     */
    protected $replied_to;
    
    /** @var string */
    protected $created_at;
    
    /** @var string */
    protected $updated_at;
    
    /**
     * @return int
     */
    public function getTicketId()
    {
        return $this->ticket_id;
    }
    
    /**
     * @param int $ticket_id
     *
     * @return TicketReply
     */
    public function setTicketId($ticket_id)
    {
        $this->ticket_id = $ticket_id;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }
    
    /**
     * @param string $body
     *
     * @return TicketReply
     */
    public function setBody($body)
    {
        $this->body = $body;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getBodyText()
    {
        return $this->body_text;
    }
    
    /**
     * @param string $body_text
     *
     * @return TicketReply
     */
    public function setBodyText($body_text)
    {
        $this->body_text = $body_text;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getFromEmail()
    {
        return $this->from_email;
    }
    
    /**
     * @param string $from_email
     *
     * @return TicketReply
     */
    public function setFromEmail($from_email)
    {
        $this->from_email = $from_email;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }
    
    /**
     * @param int $user_id
     *
     * @return TicketReply
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getCcEmails()
    {
        return $this->cc_emails;
    }
    
    /**
     * @param array $cc_emails
     *
     * @return TicketReply
     */
    public function setCcEmails($cc_emails)
    {
        $this->cc_emails = $cc_emails;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getBccEmails()
    {
        return $this->bcc_emails;
    }
    
    /**
     * @param array $bcc_emails
     *
     * @return TicketReply
     */
    public function setBccEmails($bcc_emails)
    {
        $this->bcc_emails = $bcc_emails;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getRepliedTo()
    {
        return $this->replied_to;
    }
    
    /**
     * @param array $replied_to
     *
     * @return TicketReply
     */
    public function setRepliedTo($replied_to)
    {
        $this->replied_to = $replied_to;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * @param string $created_at
     *
     * @return TicketReply
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * @param string $updated_at
     *
     * @return TicketReply
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        
        return $this;
    }
}