<?php

namespace Freshdesk;

/**
 * Class Config
 * @package Freshdesk
 */
class Config
{
    /** @var string */
    protected $domain;

    /** @var string */
    protected $apiKey;

    /** @var string */
    protected $apiUrl;
    
    /** @var bool */
    protected $customUrl = false;

    /** @var array */
    protected $requiredSettings = ['domain', 'apiKey'];
    
    /** @var int */
    protected $connectionTimeout = 30;//sec

    const APIV2_URL = 'api/v2/';
    const URL_FORMAT = 'https://%s.freshdesk.com/%s';
    const PASSWORD = 'x';

    /**
     * Config constructor.
     *
     * @param array $settings
     *
     * @throws \Exception if required property is empty
     */
    public function __construct($settings)
    {
        foreach($settings as $property => $value) {
            $setterMethod = 'set' . ucfirst($property);

            if (method_exists($this, $setterMethod)) {
                $this->$setterMethod($value);
            }
        }

        foreach ($this->requiredSettings as $requiredSetting) {
            if (empty($this->{$requiredSetting})) {
                throw new \Exception('Empty required property ' . $requiredSetting);
            }
        }

        $this->setApiUrl($this->domain);
    }
    
    /**
     * @param string $domain
     *
     * @return $this
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }
    
    /**
     * @param string $apiKey
     *
     * @return $this
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }
    
    /**
     * @param int $customUrl
     *
     * @return $this
     */
    public function setCustomUrl($customUrl)
    {
        $this->customUrl = (bool)$customUrl;
        
        return $this;
    }

    /**
     * @return bool
     */
    public function getCustomUrl()
    {
        return $this->customUrl;
    }
    
    /**
     * @param string $domain
     *
     * @return $this
     */
    public function setApiUrl($domain)
    {
        $this->apiUrl = $this->customUrl ? $domain : $this->makeUrl($domain, self::APIV2_URL);
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getApiUrl()
    {
        return $this->apiUrl;
    }
    
    /**
     * @param int $connectionTimeout
     *
     * @return $this
     */
    public function setConnectionTimeout($connectionTimeout)
    {
        $this->connectionTimeout = $connectionTimeout;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getConnectionTimeout()
    {
        return $this->connectionTimeout;
    }

    /**
     * @param string $domain
     * @param string $path
     *
     * @return string
     */
    protected function makeUrl($domain, $path)
    {
        return sprintf(self::URL_FORMAT, $domain, $path);
    }
}