<?php

namespace Freshdesk\Managers;

use Freshdesk\Models\BaseModel;
use Freshdesk\Models\Contact;

/**
 * Class ContactManager
 * @package Freshdesk\Managers
 */
class ContactManager extends BaseManager
{
    const OBJECT_URL = 'contacts';
    const FILTER_EMAIL = 'email';
    
    /**
     * @param BaseModel $data
     *
     * @return Contact
     */
    public function create($data)
    {
        return parent::create($data);
    }
    
    /**
     * @param array $filters
     *
     * @return Contact[]
     */
    public function getList($filters = array())
    {
        return parent::getList($filters);
    }
}