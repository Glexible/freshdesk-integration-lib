<?php

namespace Freshdesk\Models\SubModels;

use Freshdesk\Models\AbstractModel;

/**
 * Class SubModelBase
 * @package Freshdesk\Models\SubModels
 */
class SubModelBase extends AbstractModel
{
    /**
     * SubModelBase constructor.
     *
     * @param array|object|null $data
     */
    public function __construct($data = null)
    {
        if ($data !== null) {
            foreach ($data as $propertyName => $value) {
                $this->setProperty($propertyName, $value);
            }
        }
    }

    /**
     * @return array
     */
    public function exportNotEmpty()
    {
        $output = [];
        $reflect = new \ReflectionClass($this);
        $properties = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);
        
        foreach ($properties as $property) {
            $name = $property->getName();
            $item = $this->{$name};
            
            if (!empty($item)) {
                $output[$name] = $this->exportItem($item);
            }
        }

        return $output;
    }
    
    /**
     * @param mixed $item
     *
     * @return array
     */
    protected function exportItem($item)
    {
        if (is_object($item) && method_exists($item, 'exportNotEmpty')) {
            return $item->exportNotEmpty();
        }
        
        if (is_array($item)) {
            $output = [];
        
            foreach ($item as $value) {
                if (!empty($value)) {
                    $output[] = $this->exportItem($value);
                }
            }
            
            return $output;
        }
        
        return $item;
    }
}