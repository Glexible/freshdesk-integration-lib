<?php

namespace Freshdesk\Managers;

use Freshdesk\Models\Ticket;

/**
 * Class TicketManager
 * @package Freshdesk\Managers
 */
class TicketManager extends BaseManager
{
    const OBJECT_URL = 'tickets';
    
    const FILTER = 'filter';
    const FILTER_REQUESTER_ID = 'requester_id';
    const FILTER_EMAIL = 'email';
    const FILTER_COMPANY_ID = 'company_id';
    const FILTER_UPDATED_SINCE = 'updated_since';
    const FILTER_PAGE = 'page';
    const FILTER_PER_PAGE = 'per_page';
    
    const ORDER_BY_STATUS = 'status';
    
    const PER_PAGE = 50;
    
    /**
     * @param array $filters
     *
     * @return Ticket[]
     */
    public function getList($filters = array())
    {
        return parent::getList($filters);
    }
}