<?php

namespace Freshdesk\Models;

use Freshdesk\Models\SubModels\KeyValue;
use Freshdesk\Models\Traits\Attachments;

/**
 * Class Ticket
 * @package Freshdesk\Models
 */
class Ticket extends BaseModel
{
    use Attachments;
    
    /**
     * Email address added in the 'cc' field of the incoming ticket email
     *
     * @var array
     */
    protected $cc_emails;
    
    /** @var int */
    protected $company_id;
    
    /**
     * Key value pairs containing the names and values of custom fields.
     *
     * @var KeyValue[]
     */
    protected $custom_fields;
    
    /**
     * Set to true if the ticket has been deleted/trashed.
     * Deleted tickets will not be displayed in any views except the "deleted" filter
     *
     * @var bool
     */
    protected $deleted;
    
    /**
     * HTML content of the ticket
     *
     * @var string
     */
    protected $description;
    
    /** @var string */
    protected $description_text;
    
    /**
     * Timestamp that denotes when the ticket is due to be resolved
     *
     * @var string
     */
    protected $due_by;
    
    /**
     * Email address of the requester. If no contact exists with this email address in Freshdesk,
     * it will be added as a new contact.
     *
     * @var string
     */
    protected $email;
    
    /** @var int */
    protected $email_config_id;
    
    /**
     * Facebook ID of the requester. A contact should exist with this facebook_id in Freshdesk.
     *
     * @var string
     */
    protected $facebook_id;
    
    /**
     * Timestamp that denotes when the first response is due
     *
     * @var string
     */
    protected $fr_due_by;
    
    /**
     * Set to true if the ticket has been escalated as the result of first response time being breached
     *
     * @var bool
     */
    protected $fr_escalated;
    
    /**
     * Email address(e)s added while forwarding a ticket
     *
     * @var array
     */
    protected $fwd_emails;
    
    /**
     * ID of the group to which the ticket has been assigned
     *
     * @var int
     */
    protected $group_id;
    
    /** @var bool */
    protected $is_escalated;
    
    /**
     * Name of the requester
     *
     * @var string
     */
    protected $name;
    
    /**
     * Phone number of the requester. If no contact exists with this phone number in Freshdesk,
     * it will be added as a new contact. If the phone number is set and the email address is not,
     * then the name attribute is mandatory.
     *
     * @var string
     */
    protected $phone;
    
    /**
     * Enum value from PRIORITY_* constants list
     *
     * @var int
     */
    protected $priority = self::PRIORITY_LOW;

    /**
     * ID of the product to which the ticket is associated
     *
     * @var int
     */
    protected $product_id;
    
    /**
     * Email address added while replying to a ticket
     *
     * @var array
     */
    protected $reply_cc_emails;
    
    /**
     * User ID of the requester. For existing contacts, the requester_id can be passed instead of the requester's email.
     *
     * @var int
     */
    protected $requester_id;
    
    /**
     * ID of the agent to whom the ticket has been assigned
     *
     * @var int
     */
    protected $responder_id;
    
    /**
     * The channel through which the ticket was created. Enum value from SOURCE_* constants list
     *
     * @var int
     */
    protected $source = self::SOURCE_EMAIL;
    
    /**
     * Set to true if the ticket has been marked as spam
     *
     * @var bool
     */
    protected $spam;
    
    /**
     * Enum value from STATUS_* constants list
     *
     * @var int
     */
    protected $status = self::STATUS_OPEN;
    
    /** @var string */
    protected $subject;
    
    /**
     * Tags that have been associated with the ticket
     *
     * @var array
     */
    protected $tags;
    
    /**
     * Email addresses to which the ticket was originally sent
     *
     * @var array
     */
    protected $to_emails;
    
    /**
     * Twitter handle of the requester.
     * If no contact exists with this handle in Freshdesk, it will be added as a new contact
     *
     * @var string
     */
    protected $twitter_id;
    
    /**
     * Helps categorize the ticket according to the different kinds of issues your support team deals with.
     *
     * @var string|null
     */
    protected $type = null;
    
    /** @var string */
    protected $created_at;
    
    /** @var string */
    protected $updated_at;
    
    const SOURCE_EMAIL = 1;
    const SOURCE_PORTAL = 2;
    const SOURCE_PHONE = 3;
    const SOURCE_CHAT = 7;
    const SOURCE_MOBIHELP = 8;
    const SOURCE_FEEDBACK_WIDGET = 9;
    const SOURCE_OUTBOUND_EMAIL = 10;
    
    const STATUS_OPEN = 2;
    const STATUS_PENDING = 3;
    const STATUS_RESOLVED = 4;
    const STATUS_CLOSED = 5;
    
    const PRIORITY_LOW = 1;
    const PRIORITY_MEDIUM = 2;
    const PRIORITY_HIGH = 3;
    const PRIORITY_URGENT = 4;
    
    /**
     * @return array
     */
    public function getCcEmails()
    {
        return $this->cc_emails;
    }
    
    /**
     * @param array $cc_emails
     *
     * @return Ticket
     */
    public function setCcEmails($cc_emails)
    {
        $this->cc_emails = $cc_emails;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }
    
    /**
     * @param int $company_id
     *
     * @return Ticket
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getCustomFields()
    {
        return $this->custom_fields;
    }
    
    /**
     * @param array $customFields
     *
     * @return Ticket
     */
    public function setCustomFields($customFields)
    {
        foreach ($customFields as $key => $value) {
            if (empty($value)) {
                continue;
            }
    
            $this->addCustomField($key, $value);
        }
        
        return $this;
    }
    
    /**
     * @param $key
     * @param $value
     *
     * @return $this
     */
    public function addCustomField($key, $value)
    {
        $customField = new KeyValue;
        $customField->key = $key;
        $customField->value = $value;
        $this->custom_fields[] = $customField;
        
        return $this;
    }
    
    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasCustomField($key)
    {
        foreach ($this->custom_fields as $customField) {
            if ($customField->key == $key && !empty($customField->value)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * @return bool
     */
    public function isDeleted()
    {
        return boolval($this->deleted);
    }
    
    /**
     * @param bool $deleted
     *
     * @return Ticket
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * @param string $description
     *
     * @return Ticket
     */
    public function setDescription($description)
    {
        $this->description = $description;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getDescriptionText()
    {
        return $this->description_text;
    }
    
    /**
     * @param string $description_text
     *
     * @return Ticket
     */
    public function setDescriptionText($description_text)
    {
        $this->description_text = $description_text;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getDueBy()
    {
        return $this->due_by;
    }
    
    /**
     * @param string $due_by
     *
     * @return Ticket
     */
    public function setDueBy($due_by)
    {
        $this->due_by = $due_by;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    /**
     * @param string $email
     *
     * @return Ticket
     */
    public function setEmail($email)
    {
        $this->email = $email;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getEmailConfigId()
    {
        return $this->email_config_id;
    }
    
    /**
     * @param int $email_config_id
     *
     * @return Ticket
     */
    public function setEmailConfigId($email_config_id)
    {
        $this->email_config_id = $email_config_id;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }
    
    /**
     * @param string $facebook_id
     *
     * @return Ticket
     */
    public function setFacebookId($facebook_id)
    {
        $this->facebook_id = $facebook_id;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getFrDueBy()
    {
        return $this->fr_due_by;
    }
    
    /**
     * @param string $fr_due_by
     *
     * @return Ticket
     */
    public function setFrDueBy($fr_due_by)
    {
        $this->fr_due_by = $fr_due_by;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isFrEscalated()
    {
        return $this->fr_escalated;
    }
    
    /**
     * @param bool $fr_escalated
     *
     * @return Ticket
     */
    public function setFrEscalated($fr_escalated)
    {
        $this->fr_escalated = $fr_escalated;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getFwdEmails()
    {
        return $this->fwd_emails;
    }
    
    /**
     * @param array $fwd_emails
     *
     * @return Ticket
     */
    public function setFwdEmails($fwd_emails)
    {
        $this->fwd_emails = $fwd_emails;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->group_id;
    }
    
    /**
     * @param int $group_id
     *
     * @return Ticket
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isIsEscalated()
    {
        return $this->is_escalated;
    }
    
    /**
     * @param bool $is_escalated
     *
     * @return Ticket
     */
    public function setIsEscalated($is_escalated)
    {
        $this->is_escalated = $is_escalated;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     *
     * @return Ticket
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }
    
    /**
     * @param string $phone
     *
     * @return Ticket
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }
    
    /**
     * @param int $priority
     *
     * @return Ticket
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->product_id;
    }
    
    /**
     * @param int $product_id
     *
     * @return Ticket
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getReplyCcEmails()
    {
        return $this->reply_cc_emails;
    }
    
    /**
     * @param array $reply_cc_emails
     *
     * @return Ticket
     */
    public function setReplyCcEmails($reply_cc_emails)
    {
        $this->reply_cc_emails = $reply_cc_emails;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getRequesterId()
    {
        return $this->requester_id;
    }
    
    /**
     * @param int $requester_id
     *
     * @return Ticket
     */
    public function setRequesterId($requester_id)
    {
        $this->requester_id = $requester_id;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getResponderId()
    {
        return $this->responder_id;
    }
    
    /**
     * @param int $responder_id
     *
     * @return Ticket
     */
    public function setResponderId($responder_id)
    {
        $this->responder_id = $responder_id;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getSource()
    {
        return $this->source;
    }
    
    /**
     * @param int $source
     *
     * @return Ticket
     */
    public function setSource($source)
    {
        $this->source = $source;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isSpam()
    {
        return $this->spam;
    }
    
    /**
     * @param bool $spam
     *
     * @return Ticket
     */
    public function setSpam($spam)
    {
        $this->spam = $spam;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function isClosed()
    {
        return $this->status == self::STATUS_CLOSED;
    }
    
    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * @param int $status
     *
     * @return Ticket
     */
    public function setStatus($status)
    {
        $this->status = $status;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }
    
    /**
     * @param string $subject
     *
     * @return Ticket
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }
    
    /**
     * @param array $tags
     *
     * @return Ticket
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getToEmails()
    {
        return $this->to_emails;
    }
    
    /**
     * @param array $to_emails
     *
     * @return Ticket
     */
    public function setToEmails($to_emails)
    {
        $this->to_emails = $to_emails;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getTwitterId()
    {
        return $this->twitter_id;
    }
    
    /**
     * @param string $twitter_id
     *
     * @return Ticket
     */
    public function setTwitterId($twitter_id)
    {
        $this->twitter_id = $twitter_id;
        
        return $this;
    }
    
    /**
     * @return null|string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * @param null|string $type
     *
     * @return Ticket
     */
    public function setType($type)
    {
        $this->type = $type;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * @param string $created_at
     *
     * @return Ticket
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * @param string $updated_at
     *
     * @return Ticket
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        
        return $this;
    }
}