<?php

namespace Freshdesk\Managers;

use Freshdesk\Models\TicketConversation;

/**
 * Class TicketConversationManager
 * @package Freshdesk\Managers
 */
class TicketConversationManager extends TicketParentManager
{
    const OBJECT_URL = '/%d/conversations';
    
    /**
     * @param array $filters  There are no filters for conversation list
     *
     * @return TicketConversation[]
     */
    public function getList($filters = array())
    {
        return parent::getList();
    }
}