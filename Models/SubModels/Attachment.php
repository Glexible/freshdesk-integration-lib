<?php

namespace Freshdesk\Models\SubModels;

/**
 * Class Attachment
 * @package Freshdesk\Models\SubModels
 */
class Attachment extends SubModelBase
{
    /** @var int */
    public $id;
    
    /** @var string */
    public $content_type;
    
    /** @var int */
    public $file_size;
    
    /** @var string */
    public $name;
    
    /** @var string */
    public $attachment_url;
    
    /**
     * It's option for creation only
     *
     * @var string
     */
    public $pathToFile;
    
    /**
     * It's option for creation only
     *
     * @var mixed
     */
    public $fileContent;
    
    /** @var string */
    public $created_at;
    
    /** @var string */
    public $updated_at;
}