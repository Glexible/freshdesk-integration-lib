<?php

namespace Freshdesk\Managers;

/**
 * Class TicketParentManager
 * @package Freshdesk\Managers
 */
class TicketParentManager extends BaseManager
{
    /**
     * @param string $objectUrl
     */
    public function setObjectUrl($objectUrl)
    {
        parent::setObjectUrl(TicketManager::OBJECT_URL . $objectUrl);
    }
}