<?php

namespace Freshdesk\Models;

/**
 * Class Conversation
 * @package Freshdesk\Models
 */
class Conversation extends BaseModel
{
    /**
     * Attachments associated with the conversation. The total size of all of a ticket's attachments cannot exceed 15MB.
     *
     * @var array
     */
    protected $attachments;
    
    /**
     * Content of the conversation in HTML
     *
     * @var string
     */
    protected $body;
    
    /**
     * Content of the conversation in plain text
     *
     * @var string
     */
    protected $body_text;
    
    /**
     * Set to true if a particular conversation should appear as being created from outside (i.e., not through web portal)
     *
     * @var bool
     */
    protected $incoming;
    
    /**
     * Email addresses of agents/users who need to be notified about this conversation
     *
     * @var array
     */
    protected $to_emails;
    
    /**
     * Set to true if the note is private
     *
     * @var bool
     */
    protected $private;
    
    /**
     * Denotes the type of the conversation.
     *
     * @var int
     */
    protected $source;
    
    /**
     * Email address from which the reply is sent. For notes, this value will be null.
     *
     * @var string|null
     */
    protected $support_email;
    
    /**
     * ID of the ticket to which this conversation is being added
     *
     * @var int
     */
    protected $ticket_id;
    
    /**
     * ID of the agent/user who is adding the conversation
     *
     * @var int
     */
    protected $user_id;
    
    /** @var string */
    protected $created_at;
    
    /** @var string */
    protected $updated_at;
    
    /**
     * @return array
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
    
    /**
     * @param array $attachments
     *
     * @return Conversation
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }
    
    /**
     * @param string $body
     *
     * @return Conversation
     */
    public function setBody($body)
    {
        $this->body = $body;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getBodyText()
    {
        return $this->body_text;
    }
    
    /**
     * @param string $body_text
     *
     * @return Conversation
     */
    public function setBodyText($body_text)
    {
        $this->body_text = $body_text;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isIncoming()
    {
        return $this->incoming;
    }
    
    /**
     * @param bool $incoming
     *
     * @return Conversation
     */
    public function setIncoming($incoming)
    {
        $this->incoming = $incoming;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getToEmails()
    {
        return $this->to_emails;
    }
    
    /**
     * @param array $to_emails
     *
     * @return Conversation
     */
    public function setToEmails($to_emails)
    {
        $this->to_emails = $to_emails;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isPrivate()
    {
        return $this->private;
    }
    
    /**
     * @param bool $private
     *
     * @return Conversation
     */
    public function setPrivate($private)
    {
        $this->private = $private;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getSource()
    {
        return $this->source;
    }
    
    /**
     * @param int $source
     *
     * @return Conversation
     */
    public function setSource($source)
    {
        $this->source = $source;
        
        return $this;
    }
    
    /**
     * @return null|string
     */
    public function getSupportEmail()
    {
        return $this->support_email;
    }
    
    /**
     * @param null|string $support_email
     *
     * @return Conversation
     */
    public function setSupportEmail($support_email)
    {
        $this->support_email = $support_email;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getTicketId()
    {
        return $this->ticket_id;
    }
    
    /**
     * @param int $ticket_id
     *
     * @return Conversation
     */
    public function setTicketId($ticket_id)
    {
        $this->ticket_id = $ticket_id;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }
    
    /**
     * @param int $user_id
     *
     * @return Conversation
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * @param string $created_at
     *
     * @return Conversation
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * @param string $updated_at
     *
     * @return Conversation
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        
        return $this;
    }
}