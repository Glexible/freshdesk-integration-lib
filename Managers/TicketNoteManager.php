<?php

namespace Freshdesk\Managers;

/**
 * Class TicketNoteManager
 * @package Freshdesk\Managers
 */
class TicketNoteManager extends TicketParentManager
{
    const OBJECT_URL = '/%d/notes';
}