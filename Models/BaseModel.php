<?php

namespace Freshdesk\Models;

use Freshdesk\Models\SubModels\KeyValue;
use Freshdesk\Models\SubModels\SubModelBase;

/**
 * Class BaseModel
 * @package Zendesk\Models
 */
class BaseModel extends AbstractModel
{
    /** @var int */
    protected $id;
    
    /** @var string */
    protected $parentModelName;
    
    /** @var int */
    protected $parentObjectId;

    /** @var array */
    protected $excludeExportProperties = [
        'id',
        'parentModelName',
        'parentObjectId',
        'excludeExportProperties',
        'attachments',
        'addExcludeExportProperties',
        'created_at',
        'updated_at'
    ];

    /** @var array */
    protected $addExcludeExportProperties = [];

    /**
     * DeskModels constructor.
     *
     * Send data into constructor for creation object from non Desk response
     *
     * @param array|object|null $data
     */
    public function __construct($data = null)
    {
        if (is_array($data) || is_object($data)) {
            $this->fillObject($data);
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|string $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = (int)$id;

        return $this;
    }
    
    /**
     * @return array
     */
    public function getExcludedProperties()
    {
        return array_merge($this->excludeExportProperties, $this->addExcludeExportProperties);
    }

    /**
     * @param array|object $data
     *
     * @return $this
     */
    public function fillObject($data)
    {
        foreach ($data as $key => $value) {
            $this->setProperty($key, $value);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function exportData()
    {
        $output = [];
        $reflect = new \ReflectionClass($this);
        $properties = $reflect->getProperties(\ReflectionProperty::IS_PROTECTED);

        foreach ($properties as $property) {
            $propertyName = $property->getName();
            
            if (in_array($propertyName, $this->getExcludedProperties()) || empty($this->{$propertyName})) {
                continue;
            }
    
            $output[$propertyName] = $this->exportItem($this->{$propertyName});
        }

        return $output;
    }
    
    /**
     * @param mixed $item
     *
     * @return mixed
     */
    protected function exportItem($item)
    {
        if ($item instanceof self) {
            return $item->exportData();
        }
        
        if ($item instanceof SubModelBase) {
            return $item->exportNotEmpty();
        }
        
        if (is_array($item)) {
            return $this->exportArray($item);
        }
        
        return $item;
    }
    
    /**
     * @param array $dataArray
     *
     * @return array
     */
    protected function exportArray($dataArray)
    {
        $exportedData = [];
        
        foreach ($dataArray as $item) {
            if ($item instanceof KeyValue) {
                $exportedData[$item->key] = $item->value;
            } else {
                $exportedData[] = $this->exportItem($item);
            }
        }
        
        return $exportedData;
    }
    
    /**
     * @param int $parentObjectId
     *
     * @return $this
     */
    public function setParentObjectId($parentObjectId)
    {
        $this->parentObjectId = $parentObjectId;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getParentObjectId()
    {
        return $this->parentObjectId;
    }
    
    /**
     * @param string $parentModelName
     *
     * @return $this
     */
    public function setParentModelName($parentModelName)
    {
        $this->parentModelName = $parentModelName;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getParentModelName()
    {
        return $this->parentModelName;
    }
    
    /**
     * @return bool
     */
    public function hasAttachment()
    {
        $propertyName = 'attachments';
        
        return property_exists($this, $propertyName) && !empty($this->{$propertyName});
    }
}