<?php

namespace Freshdesk\Models;

/**
 * Class AbstractModel
 * @package Freshdesk\Models
 */
abstract class AbstractModel
{
    /**
     * @param string $property
     *
     * @return null|mixed
     */
    public function get($property)
    {
        if (property_exists($this, $property)) {
            return $this->{$property};
        }

        return null;
    }

    /**
     * @param string $propertyName
     * @param mixed  $value
     *
     * @return $this
     */
    protected function setProperty($propertyName, $value)
    {
        //make camelcase method name
        $methodName = str_replace('_', '', ucwords('set_' . $propertyName, "_"));

        if (property_exists($this, $propertyName)) {
            if (method_exists($this, $methodName)) {
                $this->$methodName($value);
            } elseif (is_array($value)) {
                $this->{$propertyName} = [];
                
                foreach ($value as $item) {
                    $this->{$propertyName}[] = $item;
                }
            } else {
                $this->{$propertyName} = $value;
            }
        }

        return $this;
    }
}