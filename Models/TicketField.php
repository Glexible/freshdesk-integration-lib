<?php

namespace Freshdesk\Models;

/**
 * Class TicketField
 * @package Freshdesk\Models
 */
class TicketField extends BaseModel
{
    /** @var bool */
    protected $default;
    
    /** @var string */
    protected $description;
    
    /** @var string */
    protected $label;
    
    /** @var string */
    protected $name;
    
    /** @var int */
    protected $position;
    
    /** @var bool */
    protected $required_for_closure;
    
    /** @var string */
    protected $type;
    
    /** @var bool */
    protected $required_for_agents;
    
    /** @var bool */
    protected $required_for_customers;
    
    /** @var string */
    protected $label_for_customers;
    
    /** @var bool */
    protected $customers_can_edit;
    
    /** @var bool */
    protected $displayed_to_customers;
    
    /** @var string */
    protected $created_at;
    
    /** @var string */
    protected $updated_at;
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @param int $id
     *
     * @return TicketField
     */
    public function setId($id)
    {
        $this->id = $id;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isDefault()
    {
        return $this->default;
    }
    
    /**
     * @param bool $default
     *
     * @return TicketField
     */
    public function setDefault($default)
    {
        $this->default = $default;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * @param string $description
     *
     * @return TicketField
     */
    public function setDescription($description)
    {
        $this->description = $description;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    /**
     * @param string $label
     *
     * @return TicketField
     */
    public function setLabel($label)
    {
        $this->label = $label;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @param string $name
     *
     * @return TicketField
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }
    
    /**
     * @param int $position
     *
     * @return TicketField
     */
    public function setPosition($position)
    {
        $this->position = $position;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isRequiredForClosure()
    {
        return $this->required_for_closure;
    }
    
    /**
     * @param bool $required_for_closure
     *
     * @return TicketField
     */
    public function setRequiredForClosure($required_for_closure)
    {
        $this->required_for_closure = $required_for_closure;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * @param string $type
     *
     * @return TicketField
     */
    public function setType($type)
    {
        $this->type = $type;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isRequiredForAgents()
    {
        return $this->required_for_agents;
    }
    
    /**
     * @param bool $required_for_agents
     *
     * @return TicketField
     */
    public function setRequiredForAgents($required_for_agents)
    {
        $this->required_for_agents = $required_for_agents;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isRequiredForCustomers()
    {
        return $this->required_for_customers;
    }
    
    /**
     * @param bool $required_for_customers
     *
     * @return TicketField
     */
    public function setRequiredForCustomers($required_for_customers)
    {
        $this->required_for_customers = $required_for_customers;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getLabelForCustomers()
    {
        return $this->label_for_customers;
    }
    
    /**
     * @param string $label_for_customers
     *
     * @return TicketField
     */
    public function setLabelForCustomers($label_for_customers)
    {
        $this->label_for_customers = $label_for_customers;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isCustomersCanEdit()
    {
        return $this->customers_can_edit;
    }
    
    /**
     * @param bool $customers_can_edit
     *
     * @return TicketField
     */
    public function setCustomersCanEdit($customers_can_edit)
    {
        $this->customers_can_edit = $customers_can_edit;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isDisplayedToCustomers()
    {
        return $this->displayed_to_customers;
    }
    
    /**
     * @param bool $displayed_to_customers
     *
     * @return TicketField
     */
    public function setDisplayedToCustomers($displayed_to_customers)
    {
        $this->displayed_to_customers = $displayed_to_customers;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * @param string $created_at
     *
     * @return TicketField
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * @param string $updated_at
     *
     * @return TicketField
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        
        return $this;
    }
}