<?php

namespace Freshdesk\Models;

use Freshdesk\Models\Traits\Attachments;

/**
 * Class TicketConversation
 * @package Freshdesk\Models
 */
class TicketConversation extends BaseModel
{
    use Attachments;
    
    /** @var int */
    protected $ticket_id;
    /**
     * Content of the note in HTML format
     *
     * @var string
     */
    protected $body;
    
    /**
     * Content of the note in HTML format
     *
     * @var string
     */
    protected $body_text;
    
    /**
     * The email address from which the reply is sent. By default the global support email will be used.
     *
     * @var string
     */
    protected $from_email;
    
    /**
     * Email addresses of agents/users who need to be notified about this conversation
     *
     * @var array
     */
    protected $to_emails;
    
    /**
     * ID of the agent/user who is adding the note
     *
     * @var int
     */
    protected $user_id;
    
    /**
     * Email address added in the 'cc' field of the outgoing ticket email.
     *
     * @var array
     */
    protected $cc_emails;
    
    /**
     * Email address added in the 'bcc' field of the outgoing ticket email.
     *
     * @var array
     */
    protected $bcc_emails;
    
    /**
     * Set to true if a particular conversation should appear as being created from outside (i.e., not through web portal)
     *
     * @var bool
     */
    protected $incoming;
    
    /**
     * Set to true if a particular conversation should appear as being created from outside (i.e., not through web portal)
     *
     * @var bool
     */
    protected $private;
    
    /**
     * Denotes the type of the conversation.
     *
     * @var int
     */
    protected $source;
    
    /**
     * Email address from which the reply is sent. For notes, this value will be null.
     *
     * @var string
     */
    protected $support_email;
    
    /** @var string */
    protected $created_at;
    
    /** @var string */
    protected $updated_at;
    
    const SOURCE_REPLY = 0;
    const SOURCE_NOTE = 2;
    const SOURCE_TWEETS = 5;
    const SOURCE_SURVEY_FEEDBACK = 6;
    const SOURCE_FACEBOOK = 7;
    const SOURCE_FORWARDED_EMAILS = 8;
    const SOURCE_PHONE = 9;
    const SOURCE_MOBIHELP = 10;
    const SOURCE_E_COMMERCE = 11;
    
    /**
     * @return int
     */
    public function getTicketId()
    {
        return $this->ticket_id;
    }
    
    /**
     * @param int $ticket_id
     *
     * @return TicketConversation
     */
    public function setTicketId($ticket_id)
    {
        $this->ticket_id = $ticket_id;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }
    
    /**
     * @param string $body
     *
     * @return TicketConversation
     */
    public function setBody($body)
    {
        $this->body = $body;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getBodyText()
    {
        return $this->body_text;
    }
    
    /**
     * @param string $body_text
     *
     * @return TicketConversation
     */
    public function setBodyText($body_text)
    {
        $this->body_text = $body_text;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getFromEmail()
    {
        return $this->from_email;
    }
    
    /**
     * @param string $from_email
     *
     * @return TicketConversation
     */
    public function setFromEmail($from_email)
    {
        $this->from_email = $from_email;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getToEmails()
    {
        return $this->to_emails;
    }
    
    /**
     * @param array $to_emails
     *
     * @return TicketConversation
     */
    public function setToEmails($to_emails)
    {
        $this->to_emails = $to_emails;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }
    
    /**
     * @param int $user_id
     *
     * @return TicketConversation
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getCcEmails()
    {
        return $this->cc_emails;
    }
    
    /**
     * @param array $cc_emails
     *
     * @return TicketConversation
     */
    public function setCcEmails($cc_emails)
    {
        $this->cc_emails = $cc_emails;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getBccEmails()
    {
        return $this->bcc_emails;
    }
    
    /**
     * @param array $bcc_emails
     *
     * @return TicketConversation
     */
    public function setBccEmails($bcc_emails)
    {
        $this->bcc_emails = $bcc_emails;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isIncoming()
    {
        return $this->incoming;
    }
    
    /**
     * @param bool $incoming
     *
     * @return TicketConversation
     */
    public function setIncoming($incoming)
    {
        $this->incoming = $incoming;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isPrivate()
    {
        return $this->private;
    }
    
    /**
     * @param bool $private
     *
     * @return TicketConversation
     */
    public function setPrivate($private)
    {
        $this->private = $private;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getSource()
    {
        return $this->source;
    }
    
    /**
     * @param int $source
     *
     * @return TicketConversation
     */
    public function setSource($source)
    {
        $this->source = $source;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getSupportEmail()
    {
        return $this->support_email;
    }
    
    /**
     * @param string $support_email
     *
     * @return TicketConversation
     */
    public function setSupportEmail($support_email)
    {
        $this->support_email = $support_email;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * @param string $created_at
     *
     * @return TicketConversation
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * @param string $updated_at
     *
     * @return TicketConversation
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        
        return $this;
    }
}