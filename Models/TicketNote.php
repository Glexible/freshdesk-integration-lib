<?php

namespace Freshdesk\Models;

use Freshdesk\Models\Traits\Attachments;

/**
 * Class TicketNote
 * @package Freshdesk\Models
 */
class TicketNote extends BaseModel
{
    use Attachments;
    
    /** @var int */
    protected $ticket_id;
    /**
     * Content of the note in HTML format
     *
     * @var string
     */
    protected $body;
    
    /**
     * Content of the note in HTML format
     *
     * @var string
     */
    protected $body_text;
    
    /**
     * ID of the agent/user who is adding the note
     *
     * @var int
     */
    protected $user_id;
    
    /**
     * Email addresses of agents/users who need to be notified about this note
     *
     * @var array
     */
    protected $notify_emails;
    
    /**
     * Set to true if the note is private. The default value is true.
     *
     * @var bool
     */
    protected $private = true;
    
    /**
     * Emails array
     *
     * @var array
     */
    protected $replied_to;
    
    /** @var string */
    protected $created_at;
    
    /** @var string */
    protected $updated_at;
    
    /**
     * @return int
     */
    public function getTicketId()
    {
        return $this->ticket_id;
    }
    
    /**
     * @param int $ticket_id
     *
     * @return TicketNote
     */
    public function setTicketId($ticket_id)
    {
        $this->ticket_id = $ticket_id;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }
    
    /**
     * @param string $body
     *
     * @return TicketNote
     */
    public function setBody($body)
    {
        $this->body = $body;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getBodyText()
    {
        return $this->body_text;
    }
    
    /**
     * @param string $body_text
     *
     * @return TicketNote
     */
    public function setBodyText($body_text)
    {
        $this->body_text = $body_text;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }
    
    /**
     * @param int $user_id
     *
     * @return TicketNote
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getNotifyEmails()
    {
        return $this->notify_emails;
    }
    
    /**
     * @param array $notify_emails
     *
     * @return TicketNote
     */
    public function setNotifyEmails($notify_emails)
    {
        $this->notify_emails = $notify_emails;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isPrivate()
    {
        return $this->private;
    }
    
    /**
     * @param bool $private
     *
     * @return TicketNote
     */
    public function setPrivate($private)
    {
        $this->private = $private;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getRepliedTo()
    {
        return $this->replied_to;
    }
    
    /**
     * @param array $replied_to
     *
     * @return TicketNote
     */
    public function setRepliedTo($replied_to)
    {
        $this->replied_to = $replied_to;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    
    /**
     * @param string $created_at
     *
     * @return TicketNote
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    
    /**
     * @param string $updated_at
     *
     * @return TicketNote
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
        
        return $this;
    }

    
}