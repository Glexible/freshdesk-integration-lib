<?php

namespace Freshdesk\Managers;

/**
 * Class TicketReplyManager
 * @package Freshdesk\Managers
 */
class TicketReplyManager extends TicketParentManager
{
    const OBJECT_URL = '/%d/reply';
}