<?php

namespace Freshdesk\Managers;

use Freshdesk\Models\TicketField;

/**
 * Class TicketFieldManager
 * @package Freshdesk\Managers
 */
class TicketFieldManager extends BaseManager
{
    const OBJECT_URL = 'ticket_fields';
    
    /**
     * @param array $filters
     *
     * @return TicketField[]
     */
    public function getList($filters = array())
    {
        return parent::getList([]);
    }
}