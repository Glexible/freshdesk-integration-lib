<?php

namespace Freshdesk\Models\Traits;

use Freshdesk\Models\SubModels\Attachment;

trait Attachments
{
    /**
     * Ticket attachments. The total size of these attachments cannot exceed 15MB.
     *
     * @var Attachment[]
     */
    protected $attachments = [];
    
    /**
     * @return Attachment[]
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
    
    /**
     * @param array $attachments
     *
     * @return $this
     */
    public function setAttachments($attachments)
    {
        foreach ($attachments as $attachment) {
            $this->addAttachment(new Attachment($attachment));
        }
        
        return $this;
    }
    
    /**
     * @param Attachment $attachment
     *
     * @return $this
     */
    protected function addAttachment($attachment)
    {
        $this->attachments[] = $attachment;
        
        return $this;
    }
    
    /**
     * @param string $fileName
     * @param mixed  $fileContent
     * @param string $fileType
     *
     * @return $this
     */
    public function addNewAttachment($fileName, $fileContent, $fileType)
    {
        $attachment = new Attachment;
        $attachment->name = $fileName;
        $attachment->fileContent = $fileContent;
        $attachment->content_type = $fileType;
    
        return $this->addAttachment($attachment);
    }
}