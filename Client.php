<?php

namespace Freshdesk;

use Freshdesk\Models\SubModels\Attachment;
use Guzzle\Common\Exception\RuntimeException;
use Guzzle\Http\Exception\RequestException;
use Guzzle\Http\Message\RequestInterface;
use Guzzle\Http\Message\Response;
use Guzzle\Service\Client as GuzzleClient;

/**
 * Class Client
 * @package Freshdesk
 */
class Client
{
    /** @var string */
    protected $errorMessage = '';

    /** @var Config */
    protected $config;

    /** @var GuzzleClient */
    protected $client;

    /** @var Response */
    protected $response;

    /** @var RequestInterface */
    protected $request;
    
    const CONTENT_TYPE_APP_JSON = 'application/json';

    /** @var int */
    protected $connectionTimeout = 30;//sec

    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';
    const ACTION_SHOW = 'show';
    const ACTION_LIST = 'list';

    /** @var array */
    protected $allowedActions = array(
        self::ACTION_CREATE,
        self::ACTION_UPDATE,
        self::ACTION_DELETE,
        self::ACTION_SHOW,
        self::ACTION_LIST
    );

    /**
     * Request constructor.
     *
     * @param Config|null $config
     * @throws RuntimeException
     */
    public function __construct(Config $config = null)
    {
        if ($config !== null) {
            $this->setConfig($config);
            $this->client = $this->initClient($config);
        }
    }

    /**
     * @param Config $config
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return GuzzleClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Config $config
     *
     * @return GuzzleClient
     * @throws RuntimeException
     */
    public function initClient(Config $config)
    {
        return new GuzzleClient($config->getApiUrl());
    }

    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param string       $url
     * @param string       $action
     * @param array        $postData
     * @param int          $id
     * @param Attachment[] $attachments
     *
     * @return Response
     * @throws \Exception                     Wrong action|wrong data
     * @throws RequestException
     */
    public function execute($url, $action, array $postData = null, $id = null, $attachments = [])
    {
        if (!in_array($action, $this->allowedActions)) {
            throw new \Exception('Action not found.');
        }

        $url .= $id !== null ? '/' . $id : '';

        if ($action === self::ACTION_DELETE && $id !== null) {
            return $this->delete($url);
        }
        
        if ($action === self::ACTION_CREATE && !empty($postData) && $id === null) {
            return  $this->postPut($url, 'post', $postData, $attachments);
        }
        
        if ($action === self::ACTION_UPDATE && !empty($postData) && $id !== null) {
            return $this->postPut($url, 'put', $postData, $attachments);
        }
        
        if (($action === self::ACTION_LIST && $id === null) || ($action === self::ACTION_SHOW && $id !== null)) {
            return $this->get($url);
        }

        throw new \Exception(
            "Data error.\nAction: " . $action . 'Data: ' . var_export($postData, true) . "\nID: " . var_export($id, true)
        );
    }

    /**
     * @param string $url
     *
     * @return Response
     * @throws RequestException
     */
    public function getRaw($url)
    {
        return $this->send(
            $this->client->get($url, null, ['connect_timeout' => $this->config->getConnectionTimeout()])
        );
    }

    /**
     * @param string       $url
     * @param string       $method
     * @param array        $postData
     * @param Attachment[] $attachments
     *
     * @return Response
     * @throws RequestException
     */
    protected function postPut($url, $method, $postData, $attachments = [])
    {
        $contentType = 'application/json';
        $data = null;
        
        if (!empty($attachments)) {
            $eol = "\r\n";
            $mime_boundary = md5(time());
            $data = '';
    
            foreach ($postData as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $itemValue) {
                        $data .= $this->multipartItem($mime_boundary, $eol, $key . '[]', $itemValue);
                    }
                } else {
                    $data .= $this->multipartItem($mime_boundary, $eol, $key, $value);
                }
            }
    
            foreach ($attachments as $attachment) {
                $data .= '--' . $mime_boundary . $eol;
                $data .= 'Content-Disposition: form-data; name="attachments[]"; filename="' . $attachment->name . '"' . $eol;
                $data .= 'Content-Type: ' . $attachment->content_type . $eol . $eol;
                $data .= $attachment->fileContent . $eol;
            }
            $data .= '--' . $mime_boundary . '--' . $eol . $eol;
    
            $contentType = 'multipart/form-data; boundary=' . $mime_boundary;
        }
        
        $request = $this->client->$method(
            $url,
            ['content-type' => $contentType],
            $data,
            ['connect_timeout' => $this->config->getConnectionTimeout()]
        );
        
        if (empty($attachments)) {
            $request->setBody(json_encode($postData));
        }

        return $this->send($request);
    }
    
    /**
     * @param string $mime_boundary
     * @param string $eol
     * @param string $key
     * @param mixed  $value
     *
     * @return string
     */
    private function multipartItem($mime_boundary, $eol, $key, $value)
    {
        $data = '--' . $mime_boundary . $eol;
        $data .= 'Content-Disposition: form-data; name="' . $key . '"' . $eol . $eol;
        $data .= $value . $eol;
        
        return $data;
    }

    /**
     * @param string $url
     *
     * @return Response
     * @throws RequestException
     */
    protected function delete($url)
    {
        return $this->send(
            $this->client->delete(
                $url,
                null,
                null,
                ['connect_timeout' => $this->config->getConnectionTimeout()]
            )
        );
    }

    /**
     * @param string $url
     *
     * @return Response
     * @throws RequestException
     */
    protected function get($url)
    {
        return $this->send(
            $this->client->get($url, null, ['connect_timeout' => $this->config->getConnectionTimeout()])
        );
    }
    
    /**
     * @param RequestInterface $request
     *
     * @return Response
     * @throws RequestException
     */
    private function send($request)
    {
        $this->request = $request;
        $this->request->getCurlOptions()->set(CURLOPT_USERPWD, $this->config->getApiKey() . ':' . Config::PASSWORD);

        return $this->request->send();
    }
}