<?php

namespace Freshdesk\Models\SubModels;

/**
 * Class Avatar
 * @package Freshdesk\Models\SubModels
 */
class Avatar extends SubModelBase
{
    /** @var int */
    public $id;
    
    /** @var string */
    public $avatar_url;
    
    /** @var string */
    public $content_type;
    
    /** @var string */
    public $name;
    
    /** @var int */
    public $size;
    
    /** @var string */
    public $created_at;
    
    /** @var string */
    public $updated_at;
}